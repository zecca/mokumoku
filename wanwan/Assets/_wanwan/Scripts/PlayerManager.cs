﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        PlayerControl();	
	}

    const float MOVE_SPEED = 2;

    const float ROTATE_SPEED = 100;

    void PlayerControl()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(new Vector3(0,0,(MOVE_SPEED * Time.deltaTime)));
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(new Vector3(0, 0, -(MOVE_SPEED * Time.deltaTime)));
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(new Vector3(0, (ROTATE_SPEED * Time.deltaTime),0));
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0, -(ROTATE_SPEED * Time.deltaTime), 0));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Rigidbody rb = this.gameObject.GetComponent<Rigidbody>();
            rb.AddForce(Vector3.up * 1000);
        }
    }
}
