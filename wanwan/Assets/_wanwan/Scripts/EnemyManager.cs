﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    const int MOVE_SPEED = 10;

    /// <summary>
    /// 
    /// </summary>
    enum Phase
    {
        SEARCH,
        ROTATE,
        MOVE,
        END
    }

    private Phase m_Phase = Phase.SEARCH;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        MoveEnemy();
    }

    /// <summary>
    /// ターゲット
    /// </summary>
    Vector3 m_Target;
    const int ROTATE_SPEED = 100;
    bool Rotate()
    {
        m_RotateTime =
            (m_RotateTime - Time.deltaTime) < 0 ? 0 : (m_RotateTime - Time.deltaTime);
        //Debug.Log("rotate time :" + m_RotateTime);
        if (m_RotateTime > 0)
        {
            transform.Rotate(new Vector3(0, (Time.deltaTime * ROTATE_SPEED), 0));
            return true;
        }
        return false;
    }

    /// <summary>
    /// 敵が動きます
    /// </summary>
    void MoveEnemy()
    {
        //方向を決める
        switch (m_Phase)
        {
            case Phase.SEARCH:
                //ターゲットのプレーヤを発見します。
                var player_list = GameObject.FindGameObjectsWithTag("Player");
                int target = Random.Range(0,player_list.Length);
                m_Target = new Vector3(
                    player_list[target].transform.position.x,
                    this.transform.position.y,
                    player_list[target].transform.position.z);
                m_Phase = Phase.ROTATE;
                //回転スタート
                SetRotateTime();
                break;
            case Phase.ROTATE:
                //とりあえず回転します。
                //m_Phase
                if (!Rotate())
                {
                    transform.LookAt(m_Target);
                    //transform.Rotate();
                    m_Phase = Phase.MOVE;
                }
                break;
            case Phase.MOVE:
                RaycastHit foo = new RaycastHit();
                int layer = 1 << 10;
                var ray = new Ray(
                    new Vector3(transform.position.x, transform.position.y+3, transform.position.z)
                    ,new Vector3(0,-10,0));
                ray.origin += this.transform.forward;
                if (Physics.Raycast(ray, out foo, 10.0f,layer))
                {
                    //Debug.Log(foo.transform.name);
                    if(foo.transform.tag == "out_of_field")
                    {
                        m_Phase = Phase.SEARCH;
                    }
                }
                //ターゲットに向かって直進します
                //TODO:あとで残り時間をかける
                transform.Translate(new Vector3(0, 0, (MOVE_SPEED * Time.deltaTime)));
                break;
            default:
                break;
        }
    }

    float m_RotateTime;

    const float DEFAULT_ROTATE_TIME = 3;
    private void SetRotateTime()
    {
        float ret = DEFAULT_ROTATE_TIME;
        //ゲームマネージャから、残り時間を取得
        GameObject game_manager = GameObject.FindGameObjectsWithTag("GameManager")[0];
        int passed_time = (int)game_manager.GetComponent<GameManager>().m_PassedTime;
        var nokori = (GameManager.TIME_LIMIT - passed_time);
        //if (((GameManager.TIME_LIMIT) / 2) > nokori)
        //{
        //    //ret = 
        //}else if ()
        //{
        //}
        m_RotateTime = ret;
    }
}
