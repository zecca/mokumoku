﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    /// <summary>
    /// 制限時間
    /// </summary>
    public float m_PassedTime { get; private set; }

    enum Phase
    {
        START = 0,
        GAME,
        END,
        NUM
    }

    Phase m_Phase = Phase.START;
    
    public const int TIME_LIMIT = 60;

    /// <summary>
    /// ステージオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject m_Stage;


    private List<GameObject> m_PlayerList;
    //public static GameManager{
    //    get{

    //    }
    //}

	// Use this for initialization
	void Start () {
        m_PassedTime = TIME_LIMIT;
    }
	
	// Update is called once per frame
	void Update () {
        m_PassedTime = (m_PassedTime - Time.deltaTime) < 0 ? 0:
            (m_PassedTime - Time.deltaTime);

        //フェーズ更新
        UpdatePhase();
    }

    /// <summary>
    /// フェーズを更新します。
    /// </summary>
    public void UpdatePhase()
    {
        switch (m_Phase)
        {
            case Phase.START:
                //  スタートメッセージ。取り急ぎ文字列だけ
                StartCoroutine(DisplayMessage("START", 2.0f));
                foreach (Transform pos_obj in 
                    m_Stage.transform.Find
                    ("player_pop_position").transform)
                {
                    GameObject player = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/man"));
                    player.transform.position = pos_obj.position;
                    m_PlayerList.Add(player);
                }
                //GameObject.FindGameObjectsWithTag("");
                //プレイヤーを作成します。
                //Resources.Load("Prefabs/man");

                m_Phase = Phase.GAME;
                break;
            case Phase.GAME:
                if (m_PassedTime == 0)
                {
                    m_Phase = Phase.END; ;
                }
                break;
            case Phase.END:
                //結果出力
                break;
        }
    }

    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        //GUILayout.Label();
        //時間を表示します。
        GUILayout.BeginHorizontal();
        GUILayout.Label("残り時間:"+(int)m_PassedTime);
        GUILayout.EndHorizontal();
        //ステータス
        GUILayout.BeginHorizontal();
        GUILayout.Label("フェーズ:" + m_Phase);
        GUILayout.EndHorizontal();
    }

    IEnumerator DisplayMessage(string message,float wait)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(message);
        GUILayout.EndHorizontal();
        yield return new WaitForSeconds(wait);
    }
}
