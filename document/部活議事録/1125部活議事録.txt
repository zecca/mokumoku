開始
　持ち込み企画がある場合
	担当に議題を用意させる
　無い場合
	マリパから１つを担当が選択しておく
	達成方法からどう達成するか（なにか追加してみる？どういう風につくるべきか）とか


########################午前###################
11月25日
●作るもの
　マリパ：バッタンドミノ
●担当
　プログラム
	角谷
　デザイン
	竹田	
　プランナ
	雉鼻
●仕様面
　・最終的に４人プレイ
	
　・なんか要素たせるといいかも？
	とりあえず完成を目標に
　
　・操作
	入力だけ：１キー
	
　・追加想定
	チャージとか
	パワーメーターとか

　・角谷氏のUnity力を上げる

　・さくっと一旦オリジナルの完成を目指す

●その他
　gitどうする？
	角谷氏アカウントいるやん
		→（竹）つくったー！？
　今後のＰｃ問題
	今はトンネルのもの借り
　長期的な目的
	×プロフェッショナル
	〇スペシャリスト	→　竹
	ジェネラリスト		→　雉（これがいい）　　角　
　


########################〆##################
〇終了時間：完成度
　19：10　70％

●反省会
	今回の進捗
		プログラム：　80％
			いろいろ思うところが
			サッカー試合
			大変だったわー
			達成感あるな
			くやしい
				もっと色々自由にしたい					
		デザイン：　10％
			見た目に無頓着ありあり
			クライアントをやりたい上でもっと学習したいか

		プランナー：（先生）80％
			とりあえず完成に〜　っていうところに関してはオーバしてでも到達したし〜
			デザインプランニングができてなかった
			サウンドも
								
次回プランナー（今回のプランナーが決める）
	プランナ	：　竹
	デザイン	：　角
	プログラム	：　雉
次回作業
　GOGO!スカイダイブ

●反省会後その他
　・しゃくねつわんわんのりぽじとり修正→竹

