﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattanFalingColider : MonoBehaviour {

    public bool m_checkstart = false;
    public float speed = 0.1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (m_checkstart == true)
        {
            transform.position += transform.forward * speed;
        }

	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "End")
        {
            GameMain.instance.SetGameEnd();
            m_checkstart = false;

            return;
        }

        BattanFall bf = other.gameObject.GetComponent<BattanFall>();
        if (bf == null)
        {
            return;
        }

        bf.m_fallflag = true;

    }



}
