﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatFowardObj : MonoBehaviour {

    public GameObject prefab_gameObj = null;
    int m_count = 0;

    public Text countupText;

    public Animator hummerAnim;

    public KeyCode pushKey = KeyCode.Space;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       
        if(GameMain.instance == null)
        {
            return;
        }

        countupText.gameObject.SetActive(GameMain.instance.countview);

        if (GameMain.instance.m_state == GameMain.GameState.start)
        {
            m_count = 0;
        }

        if (GameMain.instance.timerStart == false)
        {
            return;
        }



        if (Input.GetKeyDown(pushKey))
        {
            if (prefab_gameObj == null)
            {
                Debug.Log("obj無いよ");
                return;
            }
            m_count++;
            GameObject obj = Instantiate(prefab_gameObj);
            Vector3 pos = transform.position;
            pos += transform.forward * m_count;
            obj.transform.position = pos;

            GameMain.instance.SetendObj(m_count);

            hummerAnim.SetTrigger("push_button"); 

        }

        if (countupText != null)
        {
            countupText.text = m_count.ToString();
        }

        


    }
}
