﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattanFall : MonoBehaviour {

    public bool m_fallflag = false;
    public Vector3 fallangle = new Vector3(90,0,0);
    public float speed = 3.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


        if (GameMain.instance.m_state == GameMain.GameState.start)
        {
            Destroy(this.gameObject);
        }


        if (m_fallflag == true)
        {
            //transform.Rotate(1, 0, 0);
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, fallangle, speed);
        }


	}
}
