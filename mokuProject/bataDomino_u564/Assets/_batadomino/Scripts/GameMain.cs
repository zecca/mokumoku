﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMain : MonoBehaviour {

    public static GameMain instance
    {
        get;
        private set;
    }

    public enum GameState
    {
        start,
        gamemain,
        result,
        gameend,
    }

    public GameState m_state = GameState.start;

    public float gameTimer = 0.0f;
    public bool timerStart = false;
    public Text outText;
    public float timeUp = 30.0f;
    public float autostart = 5.0f;
    public BattanFalingColider checkObj;
    public Transform endObj;
    public Animator cameraAnim;
    public bool countview = true;


    int maxcount = 0;

    public void SetendObj(int count)
    {
        if (maxcount >= count) {
            return;
        }

        maxcount = count;

       if(endObj == null)
        {
            Debug.Log("endObj無いよ");
            return;
        }
        endObj.position = Vector3.forward * count;

    }

	// Use this for initialization
	void Start () {

        instance = this;

	}
	
	// Update is called once per frame
	void Update () {

        switch (m_state)
        {
            case GameState.start:
                gameTimer += Time.deltaTime;

                if (gameTimer >= autostart)
                {

                    gameTimer = 0.0f;
                    m_state = GameState.gamemain;
                    maxcount = 0;
                }
                break;

            case GameState.gamemain:
                timerStart = true;
                gameTimer += Time.deltaTime;
                if(gameTimer >= timeUp * 0.2f)
                {
                    countview = false;
                }

                if (gameTimer >= timeUp)
                {

                    timerStart = false; //ゲームのメインタイマーは動いていない
                    gameTimer = 0.0f;
                    m_state = GameState.result;

                    if (cameraAnim != null)
                    {
                        cameraAnim.SetTrigger("go_result");
                    }
                    else
                    {
                        Debug.Log("cameraAnim無いよ");

                    }

                }
                break;


            case GameState.result:

                if (checkObj == null)
                {
                    SetGameEnd();

                    return;
                }

                checkObj.m_checkstart = true;

                break;

            case GameState.gameend:

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    m_state = GameState.start;

            
                }

                checkObj.gameObject.transform.position = new Vector3(0, 0, -2);

                break;
        }


        if(outText != null)
        {
            outText.text = "Time." + gameTimer;
            outText.text += "\nStart." + m_state;
        }

	}

    public void SetGameEnd()
    {
        m_state = GameState.gameend;

        if (cameraAnim != null)
        {
            cameraAnim.SetTrigger("go_gamemain");
        }

        countview = true;

    }

}
