﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLine : MonoBehaviour {

	[SerializeField]
	GameObject[] dereteObj;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "EnemyField")
		{
			//終了
			GameMain.inctance.SetResult();

			for(int i=0;i< dereteObj.Length;i++){
				Destroy(dereteObj[i]);
			}

		}
	}
}
