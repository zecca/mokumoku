﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{

	[SerializeField]
	Animator m_anim;

	public bool m_standby = true;
	public bool m_MoveEnd = false;

	readonly Vector3 vecUp = Vector3.up;

	// Use this for initializati
	void Start()					
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (m_standby || m_MoveEnd)
		{
			transform.position += Vector3.up * 0.1f;
		}
	}

	public void Set_MoveEnd()
	{

	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "EnemyField")
		{
			if(!m_anim){
				return;
			}
			m_anim.SetTrigger("start");
			m_standby = false;
		}
		if (other.tag == "DereteField")
		{												  
			Destroy(this.gameObject);
		}
	}
}