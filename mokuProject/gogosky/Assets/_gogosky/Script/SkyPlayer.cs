﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyPlayer : MonoBehaviour {

    [SerializeField]
    Rigidbody m_rig;

	[SerializeField]
	ParticleSystem m_particle;

	public int m_playerNum = -1;

    public float m_movePow = 2.0f;
    public float m_roolSpeed = 3.0f;

    public bool m_comeback = false;


	public bool m_standby = false;
	public int m_hp;
	public int m_point = 0;



	public AudioSource m_as;																			
	public AudioClip m_aclip;

	public KeyCode m_forward = KeyCode.W;
	KeyCode m_Left = KeyCode.A;
	KeyCode m_Right = KeyCode.D;
	KeyCode m_Back = KeyCode.S;

	public void SetActionKey(KeyCode F, KeyCode L, KeyCode R, KeyCode B){
		m_forward	= F;
		m_Left		= L;
		m_Right		= R;
		m_Back		= B;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if(-1 == m_playerNum)
		{
			return;
		}

		//if (!m_standby) {		
		//	if (Input.GetKeyDown(m_forward)){
		//		//準備完了したい
		//		GameMain.inctance.SetStartPlayer(m_playerNum);
		//	}
		//	//準備中だよ
		//	return;
		//}

		if (m_comeback){
																	
			Vector3 vec = Vector3.zero - transform.position;

			m_rig.AddForce(vec);
        }


        if (Input.GetKey(m_forward))
        {
            m_rig.AddForce(transform.forward* m_movePow);
		}
		if( Input.GetKeyDown(m_forward)){
			m_particle.Play();
		}
		if (Input.GetKeyUp(m_forward)){
			m_particle.Stop();
		}

		if (Input.GetKey(m_Back))
		{
			m_rig.AddForce(-transform.forward * m_movePow);
		}

		if (Input.GetKey(m_Left))
        {
            transform.Rotate(0, -m_roolSpeed, 0);
        }
        if (Input.GetKey(m_Right))
        {
            transform.Rotate(0, m_roolSpeed, 0);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "StageArea"){
            m_comeback = true;
        }
    }

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "StageArea")
		{
			m_comeback = false;
		}

		if (other.tag == "Coin")
		{

			m_as.PlayOneShot(m_aclip);

			//C obj = other.GetC();
			CoinParam cp = other.GetComponent<CoinParam>();

			if (cp != null){
				m_point += cp.m_point;
				Destroy(cp.gameObject);
			}

		}
	}


}
