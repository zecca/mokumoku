﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartLine : MonoBehaviour {

	[SerializeField]
	public GameObject[] m_startObj;
	
	// Use this for initialization
	void Start () {

		for (int i = 0; i < m_startObj.Length; i++)
		{
			m_startObj[i].SetActive(false);
		}

	}

	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
		
		if(other.tag == "balloon"){

			GameMain.inctance.SetStart();

			for(int i=0;i< m_startObj.Length;i++){
				m_startObj[i].SetActive(true);
			}

			Destroy(this.gameObject);

		}
	}
}
