﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMain : MonoBehaviour {

	public static GameMain inctance { get; private set; }


	int m_playerNum = 0;

	public GameObject[] m_startPoint;
	public SkyPlayer[] m_player;
[SerializeField]
	Text[] m_scoreText;

	public int[] m_ranking;
	[SerializeField]
	Text m_rankingText;

	[SerializeField]
	GameObject StartObjprefab;

	enum GameMode
	{
		Standby = 0,
		Start,
		GameMain,
		Result,
		End,
	}

	GameMode m_state;
	// Use this for initialization
	void Start () {

		inctance = this;

		for(int i=0;i< m_player.Length;i++){
			m_player[i].m_playerNum = i;
		}


		m_player[0].SetActionKey(KeyCode.W, KeyCode.A, KeyCode.D, KeyCode.S);
		m_player[1].SetActionKey(KeyCode.W, KeyCode.A, KeyCode.D, KeyCode.S);
		m_player[2].SetActionKey(KeyCode.U, KeyCode.H, KeyCode.K, KeyCode.J);
		m_player[3].SetActionKey(KeyCode.U, KeyCode.H, KeyCode.K, KeyCode.J);
	}
	
	// Update is called once per frame
	void Update () {


		for (int i = 0; i < m_player.Length; i++){

			//if (!m_player[i].m_standby)
			//{
			//	m_scoreText[i].text = "[" + m_player[i].m_forward + "] Push Lady";
			//	continue;
			//}

			m_scoreText[i].text = m_player[i].m_playerNum + ":[" + m_player[i].m_point + "]";

		}

		switch (m_state){
			case GameMode.Standby:
				m_rankingText.enabled = false;


				break;
			case GameMode.Start:
			
				break;
			case GameMode.GameMain:
			
				break;
			case GameMode.Result:
				//スコア集計

				
				for (int i = 0; i < m_player.Length; i++){


					int c = m_player[i].m_point;
					for (int j=0;j< m_ranking.Length; j++){

						if(m_ranking[j]<c){
							int t = m_ranking[j];
							m_ranking[j] = c;
							c = t;
						}

					}
				}

				string outtext = "Rank\n";
				for (int i = 0; i < m_ranking.Length; i++)
				{
					outtext += "rank"+i+":"+ m_player[i];
				}

				m_rankingText.text = outtext;

				m_rankingText.enabled = true;

				for (int i = 0; i<4 ; i++){
					m_player[i].transform.position = m_startPoint[i].transform.position;
					m_player[i].transform.rotation = m_startPoint[i].transform.rotation;
					m_player[i].m_point = 0;
				}

				m_state = GameMode.End;
					break;
			case GameMode.End:
				//表示


				if(Input.GetKeyDown( m_player[0].m_forward ))
				{
					m_state = GameMode.Standby;
					Instantiate(StartObjprefab);
				}
				break;
		}



	}
		
	public void SetStartPlayer(int pNum){
		if(m_state != GameMode.Standby){
			return;
		}
		//if(pNum )

		for (int i = 0; i < 4; i++){
			m_ranking[i] = 0;

		}

		m_player[pNum].m_standby = true;
	}

	public void SetStart(){
		if (m_state == GameMode.Standby)
		{
			return;
		}
		m_state = GameMode.GameMain;

	}
	
	public void SetResult(){
		if (m_state == GameMode.GameMain)
		{
			return;
		}
		m_state = GameMode.Result;
	}
}
